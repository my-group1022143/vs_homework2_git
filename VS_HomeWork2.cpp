﻿#include <iostream>
#include <string>

int main()
{
    std::string const first = "Hello, world!";


    std::cout << first << "\n";
    std::cout << "Length: ";
    std::cout << first.length() << "\n";
    std::cout << "First symbol: ";
    std::cout << first.front()<< "\n";
    std::cout << "Last symbol:";
    std::cout << first.back() << "\n";

}